package synntix.netexchange.utils;

import org.bukkit.inventory.ItemStack;
import synntix.netexchange.chests.NeChest;

public final class StackWithChest {
    private final NeChest neChest;
    private final ItemStack itemStack;

    public StackWithChest(NeChest neChest, ItemStack itemStack) {
        this.neChest = neChest;
        this.itemStack = itemStack;
    }

    public NeChest getNeChest() {
        return neChest;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }
}
