package synntix.netexchange.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import synntix.netexchange.commands.subcommands.*;
import synntix.netexchange.managers.NeChestManager;

import java.util.ArrayList;
import java.util.List;

public class TabCompletion implements TabCompleter {

    private NeChestManager neChestManager;
    private ArrayList<SubCommand> subCommands;

    // Constructor
    public TabCompletion(NeChestManager neChestManager, ArrayList<SubCommand> subCommands) {
        this.neChestManager = neChestManager;
        this.subCommands = subCommands;
    }

    //Methods
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {

        List<String> commands = new ArrayList<>();
        List<String> sTemp = new ArrayList<>();

        switch (args.length) {

            case 1 :
                for (SubCommand subCommand : this.getSubCommands()) {
                    sTemp.add(subCommand.getName());
                }

                dynComplete(sTemp,0,args,commands);
                return commands;
//                break;
            default:
                return commands;
        }
    }

    /**
     * Dynamically picks the matching commands
     * @param subCommList List containing all the possible command names for the argNumber th argument
     * @param argNumber The position of the argument currently typing
     * @param args All the arguments of the command
     * @param commands The list where we store the matching String for that position
     */
    private void dynComplete(List<String> subCommList, int argNumber, String[] args, List<String> commands) {
        for (String s : subCommList) {
            // If the command name is longer than what we are typing
            // and the begging matches the command name
            if (s.length() >= args[argNumber].length() && s.substring(0,args[argNumber].length()).equalsIgnoreCase(args[argNumber])) {
                commands.add(s);
            }
        }
    }

    public ArrayList<SubCommand> getSubCommands() {
        return subCommands;
    }
}
