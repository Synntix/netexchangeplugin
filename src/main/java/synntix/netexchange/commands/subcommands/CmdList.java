package synntix.netexchange.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import synntix.netexchange.chests.NeChestListInventory;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.managers.NeChestManager;

import java.util.ArrayList;
import java.util.LinkedList;

public class CmdList implements SubCommand {

    private NeChestManager neChestManager;

    public CmdList(NeChestManager neChestManager) {
        this.neChestManager = neChestManager;
    }

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getDescription() {
        return "Opens a GUI with a list of your NEChests";
    }

    @Override
    public String getSyntax() {
        return "/netex list";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length != 1) {
            player.sendMessage(ChatColor.RED + "ERROR : Bad syntax, the correct syntax is:");
            player.sendMessage(this.getSyntax());
        } else {
            LinkedList<NeChest> chestList = neChestManager.getPlayerNeChestList(player.getUniqueId());
            if (chestList.size() == 0) {
                player.sendMessage("You don't have any NEChest");
            } else {
                //List containing items to be displayed on the GUI inventory
                ArrayList<ItemStack> itemGUIList = new ArrayList<>();

                for (NeChest NEChest : chestList) {
                    itemGUIList.add(createGuiItem(Material.CHEST, NEChest.getName()));
                }

                NeChestListInventory inventory = new NeChestListInventory("Your NEChests",
                        itemGUIList, getInventoryGUISize(itemGUIList.size()));

                player.openInventory(inventory.getInventory());
            }
        }
    }

    // Nice little method to create a gui item with a custom name, and description
    private ItemStack createGuiItem(final Material material, final String name) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        // Set the name of the item
        meta.setDisplayName(name);

        item.setItemMeta(meta);

        return item;
    }

    private int getInventoryGUISize(int i) {
        int res = i + (9 - i % 9);
        if (i < 9) {
            res = 9;
        } else if (i > 54) {
            res = 54;
        }

        return res;
    }
}
