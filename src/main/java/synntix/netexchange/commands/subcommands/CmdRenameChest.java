package synntix.netexchange.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.managers.NeChestManager;

public class CmdRenameChest implements SubCommand {

    private NeChestManager neChestManager;

    public CmdRenameChest(NeChestManager neChestManager) {
        this.neChestManager = neChestManager;
    }

    @Override
    public String getName() {
        return "renamechest";
    }

    @Override
    public String getDescription() {
        return "Renames a chest you own";
    }

    @Override
    public String getSyntax() {
        return "/netex renamechest <Name>";
    }

    @Override
    public void perform(Player player, String[] args) {
        if (args.length != 2) {
            player.sendMessage(ChatColor.RED + "ERROR : Bad syntax, the correct syntax is:");
            player.sendMessage(this.getSyntax());
        } else {
            //Not sure about that 4 distance
            Block targetedBlock = player.getTargetBlockExact(4);
            Location chestLocation = targetedBlock.getLocation();

            if (targetedBlock == null) {
                player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest to rename it.");
            } else {
                if (!neChestManager.isChest(targetedBlock)) {
                    player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest to rename it.");
                } else {
                    chestLocation = neChestManager.translateLocation(chestLocation);
                    int ownership = neChestManager.isAlreadyOwned(player.getUniqueId().toString(), chestLocation);
                    if (ownership == 2) {
                        player.sendMessage(ChatColor.RED + "ERROR : Someone else owns this NEChest.");
                    } else if (ownership == 0) {
                        player.sendMessage(ChatColor.RED + "ERROR : You don't own this chest.");
                    } else {
                        NeChest neChest = neChestManager.getNeChest(chestLocation);
                        neChest.setName(args[1]);

                        player.sendMessage(ChatColor.GREEN + "This chest has been successfully renamed");
                    }
                }
            }
        }
    }
}
