package synntix.netexchange.commands.subcommands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import synntix.netexchange.NetExchange;
import synntix.netexchange.NetExchangeBot;
import synntix.netexchange.commands.SubCommand;

import java.util.regex.Pattern;

public class CmdPair implements SubCommand {

    private NetExchange plugin;
    private NetExchangeBot netExchangeBot;

    public CmdPair(NetExchange netExchange, NetExchangeBot netExchangeBot) {
        this.plugin = netExchange;
        this.netExchangeBot = netExchangeBot;
    }

    @Override
    public String getName() {
        return "pair";
    }

    @Override
    public String getDescription() {
        return "Link your Minecraft account to a Discord account";
    }

    @Override
    public String getSyntax() {
        return "/netex pair <DiscordTag>";
    }

    @Override
    public void perform(Player player, String[] args) {
        int i=0;
        for (String a : args) {
            System.out.println(i+"|"+a);
            i++;
        }
//        System.out.println(player.getUniqueId());
//        System.out.println(player.getName());
        if (args.length != 2) {
            player.sendMessage(ChatColor.RED + "ERROR : Bad syntax, the correct syntax is:");
            player.sendMessage(this.getSyntax());
        } else {
            if (Pattern.matches(".+#[0-9]{4}", args[1])) {
                String guildID = plugin.getConfig().getString("Discord.GuildToken");
                netExchangeBot.addPairing(player.getUniqueId().toString(),player.getName(),args[1],guildID);
            } else {
                player.sendMessage(ChatColor.RED + "ERROR : The argument provided is not a Discord tag");
            }
        }

    }
}
