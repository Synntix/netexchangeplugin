package synntix.netexchange.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.managers.NeChestManager;

public class AddChest implements SubCommand {

    private NeChestManager neChestManager;

    public AddChest(NeChestManager neChestManager) {
        this.neChestManager = neChestManager;
    }

    @Override
    public String getName() {
        return "addchest";
    }

    @Override
    public String getDescription() {
        return "Adds the pointed chest to your NetEx chests";
    }

    @Override
    public String getSyntax() {
        return "/netex addchest [Name]";
    }

    @Override
    public void perform(Player player, String[] args) {
        //Not sure about that 5 distance
        Block targetedBlock = player.getTargetBlockExact(5);
        Location blockLocation = targetedBlock.getLocation();

        if (targetedBlock == null) {
            player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest to add it to your NEChests.");
        } else {
            if (!neChestManager.isChest(targetedBlock)) {
                player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest to add it to your NEChests.");
            } else {
                blockLocation = neChestManager.translateLocation(blockLocation);
                int ownership = neChestManager.isAlreadyOwned(player.getUniqueId().toString(), blockLocation);
                System.out.println("[AddChest] Chest ownership = "+ownership);
                if (ownership == 2) {
                    player.sendMessage(ChatColor.RED + "ERROR : Someone already owns this NEChest.");
                } else if (ownership == 1) {
                    player.sendMessage(ChatColor.RED + "ERROR : You've already added this chest.");
                } else {
                    this.neChestManager.createNeChest(player.getUniqueId(), blockLocation);
                    player.sendMessage(ChatColor.GREEN + "This chest has been successfully added to your NEchests");
                }
            }
        }

    }
}
