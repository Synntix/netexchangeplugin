package synntix.netexchange.commands.subcommands;

import org.bukkit.entity.Player;
import synntix.netexchange.commands.SubCommand;

import java.util.ArrayList;

public class HelpCommand implements SubCommand {

    private ArrayList<SubCommand> subCommands = new ArrayList<>();

    public HelpCommand(ArrayList<SubCommand> subCommands) {
        this.subCommands = subCommands;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Displaying help";
    }

    @Override
    public String getSyntax() {
        return "/netex help";
    }

    @Override
    public void perform(Player player, String[] args) {
        for (SubCommand subCommand : this.getSubCommands()) {
            player.sendMessage(subCommand.getDescription() + " : " + subCommand.getSyntax());
        }
    }

    public ArrayList<SubCommand> getSubCommands() {
        return subCommands;
    }
}
