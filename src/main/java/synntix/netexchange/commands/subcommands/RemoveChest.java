package synntix.netexchange.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.managers.NeChestManager;

public class RemoveChest implements SubCommand {

    private NeChestManager neChestManager;

    public RemoveChest(NeChestManager neChestManager) {
        this.neChestManager = neChestManager;
    }

    @Override
    public String getName() {
        return "removechest";
    }

    @Override
    public String getDescription() {
        return "Removes the pointed chest from your NEChests";
    }

    @Override
    public String getSyntax() {
        return "netex removechest";
    }

    @Override
    public void perform(Player player, String[] args) {
        //Not sure about that 5 distance
        Block targetedBlock = player.getTargetBlockExact(5);

        if (targetedBlock == null) {
            player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest to remove it from your NEChests.");
        } else {
            if (!neChestManager.isChest(targetedBlock)) {
                player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest to remove it from your NEChests.");
            } else {
                int ownership = neChestManager.isAlreadyOwned(player.getUniqueId().toString(),targetedBlock.getLocation());
                if (ownership == 2) {
                    player.sendMessage(ChatColor.RED + "ERROR : Someone else owns this NEChest.");
                } else if (ownership == 0) {
                    player.sendMessage(ChatColor.RED + "ERROR : You don't own this chest.");
                } else {
                    neChestManager.removeNeChest(targetedBlock.getLocation());

                    player.sendMessage(ChatColor.GREEN + "This chest has been successfully removed from your NEchests");
                }
            }
        }

    }
}
