package synntix.netexchange.commands.subcommands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.managers.NeChestManager;

public class CmdStatus implements SubCommand {

    private NeChestManager neChestManager;

    public CmdStatus(NeChestManager neChestManager) {
        this.neChestManager = neChestManager;
    }

    @Override
    public String getName() {
        return "status";
    }

    @Override
    public String getDescription() {
        return "Gives NE information related to a chest";
    }

    @Override
    public String getSyntax() {
        return "/netex status";
    }

    @Override
    public void perform(Player player, String[] args) {
        //Not sure about that 5 distance
        Block targetedBlock = player.getTargetBlockExact(5);
        Location chestLocation = targetedBlock.getLocation();

        if (targetedBlock == null) {
            player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest.");
        } else {
            if (!neChestManager.isChest(targetedBlock)) {
                player.sendMessage(ChatColor.RED + "ERROR : You must aim at a chest.");
            } else {
                chestLocation = neChestManager.translateLocation(chestLocation);
                int ownership = neChestManager.isAlreadyOwned(player.getUniqueId().toString(), chestLocation);
                if (ownership == 2) {
                    player.sendMessage(ChatColor.GOLD + "Someone else owns this NEChest.");
                } else if (ownership == 0) {
                    player.sendMessage(ChatColor.GOLD + "Nobody owns this chest.");
                } else {
                    NeChest chest = neChestManager.getNeChest(chestLocation);
                    player.sendMessage(ChatColor.GOLD + "You own this chest. (" + chest.getName() + ")");
                }
            }
        }
    }
}
