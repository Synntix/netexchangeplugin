package synntix.netexchange;

import org.bukkit.plugin.java.JavaPlugin;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.commands.subcommands.*;
import synntix.netexchange.managers.CommandManager;
import synntix.netexchange.commands.TabCompletion;
import synntix.netexchange.managers.DataManager;
import synntix.netexchange.managers.MessageManager;
import synntix.netexchange.managers.NeChestManager;

import java.util.ArrayList;


public final class NetExchange extends JavaPlugin {

    private MessageManager messageManager;
    private DataManager dataManager;
    private NeChestManager neChestManager;
    private ArrayList<SubCommand> subCommands;
    private CommandManager commandManager;
    private TabCompletion tabCompletion;
    private NetExchangeBot netExchangeBot;

    @Override
    public void onEnable() {
        // Plugin startup logic
        this.messageManager = new MessageManager();
        this.dataManager= new DataManager(this, netExchangeBot);
        this.neChestManager = new NeChestManager(dataManager, messageManager);
        this.netExchangeBot = new NetExchangeBot(this, neChestManager, dataManager);

        this.subCommands = new ArrayList<>();
        subCommands.add(new HelpCommand(subCommands));
        subCommands.add(new AddChest(neChestManager));
        subCommands.add(new CmdPair(this, netExchangeBot));
        subCommands.add(new RemoveChest(neChestManager));
        subCommands.add(new CmdStatus(neChestManager));
        subCommands.add(new CmdRenameChest(neChestManager));
        subCommands.add(new CmdList(neChestManager));

        this.commandManager = new CommandManager(this, neChestManager, subCommands);
        this.tabCompletion = new TabCompletion(neChestManager, subCommands);

        getServer().getPluginManager().registerEvents(new Listeners(neChestManager), this);
        getCommand("netex").setExecutor(this.commandManager);
        getCommand("netex").setTabCompleter(tabCompletion);

        getConfig().addDefault("Discord.BotToken","Your_Discord_bot_token_here");
        getConfig().addDefault("Discord.GuildToken","Your_Discord_server_id_here");
        getConfig().options().copyDefaults(true);
        saveConfig();

        getLogger().info(getConfig().getString("Discord.BotToken"));
        getLogger().info((String) getConfig().get("Discord.BotToken"));
        try {
            netExchangeBot.run(getConfig().getString("Discord.BotToken"),"");
        } catch (javax.security.auth.login.LoginException e) {
            getLogger().warning("Discord bot initialization failed : "+e.getMessage());
        }
        getLogger().info("NetExchange has been initialized");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        netExchangeBot.getApi().shutdownNow();

        saveConfig();
        this.neChestManager.saveNeChests();
        this.netExchangeBot.savePairings();
    }

    public String getGuildID() {
        return getConfig().getString("Discord.GuildToken");
    }

    public NeChestManager getNeChestManager() {
        return this.neChestManager;
    }
}
