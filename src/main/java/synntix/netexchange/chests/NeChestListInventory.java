package synntix.netexchange.chests;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NeChestListInventory implements InventoryHolder {

    private Inventory inventory;

    public NeChestListInventory(String name, List<ItemStack> items, Integer size) {
        inventory = Bukkit.createInventory(this, size, name);

        for (ItemStack item : items) {
            inventory.addItem(item);
        }
    }

    @NotNull
    @Override
    public Inventory getInventory() {
        return this.inventory;
    }
}
