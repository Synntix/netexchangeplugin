package synntix.netexchange.chests;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class NeChest {

    private final String playerUUID;
    private final Location location;
    private String name;

    public NeChest(String playerUUID, org.bukkit.Location location) {
        this.playerUUID = playerUUID;
        this.location = location;
        this.name = "NEChest";
    }

    public NeChest(String playerUUID, org.bukkit.Location location, String name) {
        this.playerUUID = playerUUID;
        this.location = location;
        this.name = name;
    }

    public String getPlayerUUID() {
        return playerUUID;
    }

    public Location getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the inventory of a chest given its location.
     * The location must point to a chest
     * @return The chest's inventory
     */
    public ItemStack[] getContents() {
        Block block = this.getLocation().getBlock();
        Chest chest = (Chest) block.getState();

        return chest.getInventory().getContents();
    }

    public Inventory getInventory() {
        Block block = this.getLocation().getBlock();
        Chest chest = (Chest) block.getState();

        return chest.getInventory();
    }

    public Chest getChest() {
        return (Chest) this.getLocation().getBlock().getState();
    }

    public int getNbOfFreeSlots() {
        int nb = 0;
        ItemStack[] inventory = this.getContents();

        for (ItemStack itemStack : inventory) {
            if (itemStack == null) {
                nb ++;
            }
        }

        return nb;
    }


}
