package synntix.netexchange.botcommands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import synntix.netexchange.NetExchangeBot;
import synntix.netexchange.Pairing;
import synntix.netexchange.getInventoryTask;
import synntix.netexchange.managers.NeChestManager;

public class CmdList extends Command {

    private final NeChestManager neChestManager;
    private final NetExchangeBot netExchangeBot;

    public CmdList(NeChestManager neChestManager, NetExchangeBot netExchangeBot) {
        this.neChestManager = neChestManager;
        this.netExchangeBot = netExchangeBot;
        // The name of the command
        this.name = "list";
        // Alternative names
        this.aliases = new String[]{"inventory","inv"};
        // This will be used by the automatic Help command from JDA Utilities
        this.help = "Prints the content of your NetEx inventory (all your registered chests) and sends it via DM";
    }

    // This is where the actual code of the command is
    @Override
    protected void execute(CommandEvent e) {
        System.out.println("[CmdList] LIST command received from ["+e.getAuthor().getName()+"]");
        e.replyInDm("Hello\nYou requested an inventory list, please wait as we implement the feature...");
        Pairing p = netExchangeBot.getPairingByDiscordID(e.getAuthor().getId());
        if (p != null) {

            System.out.println("[CmdList] Pairing retrieved - Corresponding Minecraft UUID="+p.getMcID());

            JavaPlugin plugin = (JavaPlugin) Bukkit.getPluginManager().getPlugin("NetExchange");
            BukkitTask task = new getInventoryTask(plugin, neChestManager, p.getMcID(), e.getAuthor()).runTask(plugin);
            e.reply(":mailbox_with_mail: Sending your inventory list via DM...");

        } else {
            e.reply("Uh oh... Couldn't find your Minecraft account, did you pair it with your Discord account?");
        }

    }

}
