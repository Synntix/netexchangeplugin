package synntix.netexchange.botcommands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import synntix.netexchange.ExchangeTask;
import synntix.netexchange.NetExchange;
import synntix.netexchange.NetExchangeBot;
import synntix.netexchange.Pairing;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.managers.NeChestManager;
import synntix.netexchange.utils.StackWithChest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.regex.Pattern;

public class CmdSend extends Command {

    private final NetExchange plugin;
    private final NeChestManager neChestManager;
    private final NetExchangeBot netExchangeBot;

    public CmdSend(NetExchange plugin, NeChestManager neChestManager, NetExchangeBot netExchangeBot) {
        this.plugin = plugin;
        this.neChestManager = neChestManager;
        this.netExchangeBot = netExchangeBot;
        // The name of the command
        this.name = "send";
        // This will be used by the automatic Help command from JDA Utilities
        this.help = "Sends items to another player";
    }

    @Override
    protected void execute(CommandEvent e) {

        Pairing p = netExchangeBot.getPairingByDiscordID(e.getAuthor().getId());
        if (p != null) {
            String[] args = e.getArgs().split(" ");
            if (args.length != 2) {
                e.reply("Wrong syntax.");
            } else {
                if (!Pattern.matches("<@!.{18}>", args[0])) {
                    e.reply("You must tag the receiver.");
                } else {
                    String receiverDiscordID = args[0].substring(3,21);
                    Pairing receiver = netExchangeBot.getPairingByDiscordID(receiverDiscordID);
                    if (receiver == null) {
                        e.reply(args[0] + "is not linked to a Minecraft account");
                    } else {
                        BukkitTask exchange = new ExchangeTask(neChestManager, e, p, receiver, Material.getMaterial(args[1]), 0).runTask(plugin);
                    }
                }
            }

        } else {
            e.reply("Uh oh... Couldn't find your Minecraft account, did you pair it with your Discord account?");
        }
    }
}
