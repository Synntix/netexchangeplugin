package synntix.netexchange;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import synntix.netexchange.managers.NeChestManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

public class getInventoryTask extends BukkitRunnable {
    private final JavaPlugin plugin;
    private final NeChestManager neChestManager;
    private final String playerUUID;
    private final User discordUser;

    public getInventoryTask(JavaPlugin plugin, NeChestManager neChestManager, String playerUUID, User discordUser) {
        this.plugin = plugin;
        this.neChestManager = neChestManager;
        this.playerUUID = playerUUID;
        this.discordUser = discordUser;
    }

    @Override
    public void run() {
        // What you want to schedule goes here
        plugin.getServer().broadcastMessage("Inventory task called!");
        ArrayList<ItemStack> inventory = neChestManager.getNEInventory(this.playerUUID);
        System.out.println("[CmdList] Inventory retrieved, processing...");

        Consumer<Message> msgCallback = (sentmsg) -> {
        };
        // Open channel and send the message with the specified callback
        discordUser.openPrivateChannel().queue((channel) -> {
            // ItemStacks are limited to the item's max stack size, most of the time 64
            // Simply printing the stacks would be awful when dealing with large amounts of resources,
            // so we generate a new list with the total amount of each item
            // We'll typically need to get the amount of a specific and unique item type, so we're using a HashMap <item,amount>
            HashMap<Material, Integer> itemAmounts = new HashMap<>();
            // TODO: 13/10/2020 Replace HashMap with EnumMap ?


            for (ItemStack itemStack : inventory) {
                System.out.println(itemStack);
            }
            for (ItemStack itemStack : inventory) {
                Material m = itemStack.getType();
                if (itemAmounts.containsKey(m)) {
                    // Replace this material's current amount by (current amount + stack amount)
                    itemAmounts.replace(m, itemAmounts.get(m) + itemStack.getAmount());
                } else {
                    itemAmounts.put(m,itemStack.getAmount());
                }
            }

            // Items amounts are calculated
            for (Material m : itemAmounts.keySet()) {
                System.out.println(itemAmounts.get(m)+" x \t"+m.name());
            }
            channel.sendMessage(itemAmounts.toString()).queue();
        });

    }
}
