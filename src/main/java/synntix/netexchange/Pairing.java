package synntix.netexchange;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

import java.util.List;
import java.util.function.Consumer;

public class Pairing {
    private NetExchangeBot netExchangeBot;
    private String mcID;
    private String discordID;
    private String mcName;
    private String discordTag;

    private String guildID;
    private String messageID;

    private boolean confirmed;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public Pairing(NetExchangeBot netExchangeBot, String mcID, String mcName, String discordTag, String guildID) {
        this.netExchangeBot = netExchangeBot;
        this.mcID = mcID;
        this.discordID = "";
        this.mcName = mcName;
        this.discordTag = discordTag;
        this.guildID = guildID;
        this.messageID = "";
        this.confirmed = false;
    }

    public String getMcID() {
        return mcID;
    }

    public String getMcName() {
        return mcName;
    }

    public String getDiscordID() {
        return discordID;
    }

    public String getMessageID() {
        return messageID;
    }

    public String getGuildID() {
        return guildID;
    }

    public String getDiscordTag() {
        return discordTag;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public void setDiscordID(String discordID) {
        this.discordID = discordID;
    }

    public boolean isConfirmed() { return confirmed; }

    public void requestConfirmation() {
        List<Member> members = netExchangeBot.getApi().getGuildById(guildID).getMembers();

        if (!members.isEmpty()) {// Ensure members list isn't empty (which is normally impossible, but there may be errors retrieving the members list)
            int i=0;
            User user = members.get(i).getUser();
            // Loop on the members list to find one with the corresponding DiscordTag
            while (i+1 < members.size() && !user.getAsTag().equals(discordTag)) {
                i++;
                user = members.get(i).getUser();
            }
            // Check which condition caused the loop to exit
            if (user.getAsTag().equals(discordTag)){
                // We have our user !
                System.out.println("[Pairing] Found user "+user.getAsTag());
                // Save their ID
                this.discordID = user.getId();

                // -- Message sending --
                // We need to define the callback action first (which is gonna be executed after the message is successfully sent)
                Consumer<Message> msgCallback = (sentmsg) -> {
                    //.. and save the message ID
                    this.setMessageID(sentmsg.getId());
                    System.out.println(sentmsg.getId());
                    // Add two reactions (checkmark & cross)
                    sentmsg.addReaction("U+2705").queue();
                    sentmsg.addReaction("U+274C").queue();
                };
                // Open channel and send the message with the specified callback
                user.openPrivateChannel().queue((channel) -> channel.sendMessage("Minecraft player '"+mcName+"' wants to link their NetExchange inventory to this Discord account.\nIs that you? (:white_check_mark:/:x:)").queue(msgCallback));
                System.out.println("[Pairing] Confirmation request sent! Message ID = '"+this.messageID+"'");
            } else {
                // No user found...
                System.err.printf("[Pairing] No User with tag '%s' found in guild '%s'\n",discordTag,guildID);
            }
        } else {// Members list is empty
            System.err.println("[Pairing] No members in guild, this is most likely an issue with loadMembers() or the Members cache policy");
        }
    }

    public void confirm() {
        if (!confirmed) {
            confirmed = true;

        }
    }
}
