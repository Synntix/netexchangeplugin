package synntix.netexchange;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.*;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;
import synntix.netexchange.chests.NeChestListInventory;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.managers.NeChestManager;

import java.util.ArrayList;

public class Listeners implements Listener {

    private NeChestManager neChestManager;

    public Listeners(NeChestManager neChestManager) {
        this.neChestManager = neChestManager;
    }

    @EventHandler
    public void onChestListClick(InventoryClickEvent e) {
        if (e.getInventory().getHolder() instanceof NeChestListInventory) {
            e.setCancelled(true);
        }
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory().getHolder() instanceof NeChestListInventory) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent e) {

        final Player player = (Player) e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            //Debug
            if (e.getPlayer().getInventory().getItemInMainHand().getType() == Material.CREEPER_HEAD) {
                Block theChestBlock = e.getClickedBlock();
                        BlockState state = theChestBlock.getState();
                if (state instanceof Chest) {
                    Chest chest = (Chest) state;
                    Inventory inventory = chest.getInventory();
                    if (inventory instanceof DoubleChestInventory) {
                        DoubleChest doubleChest = (DoubleChest) inventory.getHolder();
                        System.out.println(doubleChest.getLocation());
                        // You have a double chest instance
                    }
                }
            }

        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Block block = e.getBlock();

        if (!neChestManager.isChest(block)) {
            return;
        }
        Location location = neChestManager.translateLocation(block.getLocation());
        if (!neChestManager.isNetExchangeContainer(location)) {
            return;
        }

        String playerUUID = e.getPlayer().getUniqueId().toString();
        NeChest neChest = neChestManager.getNeChest(location);
        if (!neChest.getPlayerUUID().equals(playerUUID)) {
            e.getPlayer().sendMessage(ChatColor.RED + "You can't destroy this chest because someone have claimed it as a NEChest");
            e.setCancelled(true);

        } else {
            neChestManager.removeNeChest(location);
        }

    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Block block = e.getBlock();

        if (!neChestManager.isChest(block)) {
            return;
        }
        ArrayList<Location> blocksAround = new ArrayList<>();
        blocksAround.add(block.getLocation().add(1,0,0));
        blocksAround.add(block.getLocation().add(-1,0,0));
        blocksAround.add(block.getLocation().add(0,0,1));
        blocksAround.add(block.getLocation().add(0,0,-1));

        int i = 0;
        while (i < 4 && !neChestManager.isNetExchangeContainer(blocksAround.get(i))) {
            i++;
        }
        if (i != 4 && !e.getPlayer().isSneaking()) {
            e.getPlayer().sendMessage(ChatColor.RED + "[NetExchange] You cannot make a double chest with an existing NeChest");
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onBlockExplode(EntityExplodeEvent e) {
        int i = 0;
        boolean neChestInExplosion = false;
        while (i < e.blockList().size() && !neChestInExplosion) {
            if (neChestManager.isChest(e.blockList().get(i))) {
                if (neChestManager.isNetExchangeContainer(e.blockList().get(i).getLocation())) {
                    neChestInExplosion = true;
                }
            }
            i++;
        }
        if (neChestInExplosion) {
            e.setCancelled(true);
        }
    }
}
