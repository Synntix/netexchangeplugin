package synntix.netexchange;

import com.jagrosh.jdautilities.command.CommandEvent;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.exceptions.ExchangeException;
import synntix.netexchange.managers.NeChestManager;
import synntix.netexchange.utils.StackWithChest;

import java.util.ArrayList;
import java.util.UUID;

public class ExchangeTask extends BukkitRunnable {

    private NeChestManager neChestManager;
    private CommandEvent event;
    private Pairing sender;
    private Pairing receiver;
    private Material materialToExchange;
    private int amountToExchange;

    public ExchangeTask(NeChestManager neChestManager, CommandEvent event, Pairing sender, Pairing receiver, Material materialToExchange, int amountToExchange) {
        this.neChestManager = neChestManager;
        this.event = event;
        this.sender = sender;
        this.receiver = receiver;
        this.materialToExchange = materialToExchange;
        this.amountToExchange = amountToExchange;
    }

    @Override
    public void run() {
        ArrayList<StackWithChest> stacksToExchange = neChestManager.getItemStackInNeInventory(UUID.fromString(sender.getMcID()), materialToExchange);
        String receiverName = receiver.getDiscordTag().substring(0, receiver.getDiscordTag().length()-5);

        ArrayList<NeChest> receiverNeChests = neChestManager.getNeChestsWithFreeSpace(UUID.fromString(receiver.getMcID()));
        if (stacksToExchange.size() == 0) {
            event.reply("You don't have any " + materialToExchange.toString());
        } else if (getItemNumber(stacksToExchange) < amountToExchange) {
            event.reply("You don't have enough " + materialToExchange.toString());
        } else if (receiverNeChests.size() == 0) {
            event.reply(receiverName + " has not enough space in his NeChests");
        } else {
            Chest receiverChest = receiverNeChests.get(0).getChest();
            Chest senderChest = stacksToExchange.get(0).getNeChest().getChest();
            ItemStack stackToExchange = stacksToExchange.get(0).getItemStack();
            try {
                neChestManager.exchangeItemStack(senderChest, receiverChest, stackToExchange);
                event.reply(materialToExchange.toString() + " was sent successfully to " + receiverName);
            } catch (ExchangeException e) {
                e.printStackTrace();
            }
        }
    }

    private int getItemNumber (ArrayList<StackWithChest> stacks) {
        int nb = 0;
        for (StackWithChest stack : stacks) {
            nb += stack.getItemStack().getAmount();
        }

        return nb;
    }

    private int getNbOfFreeSlots(ArrayList<NeChest> neChests) {
        int nb = 0;
        for (NeChest neChest : neChests) {
            nb += neChest.getNbOfFreeSlots();
        }

        return nb;
    }
}
