package synntix.netexchange.exceptions;

public class ExchangeException extends Exception {

    public ExchangeException() {
    }

    public ExchangeException(String message) {
        super(message);
    }
}
