package synntix.netexchange.managers;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import synntix.netexchange.NetExchange;
import synntix.netexchange.commands.SubCommand;
import synntix.netexchange.commands.subcommands.*;

import java.util.ArrayList;

public class CommandManager implements CommandExecutor {

    // Attributes
    private NetExchange plugin;
    private NeChestManager neChestManager;
    private ArrayList<SubCommand> subCommands;

    // Constructor
    public CommandManager(NetExchange plugin, NeChestManager neChestManager, ArrayList<SubCommand> subCommands) {
        this.plugin = plugin;
        this.neChestManager = neChestManager;
        this.subCommands = subCommands;
    }

    // Methods

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {

            Player player = (Player) sender;

            if (args.length == 0) {
                SubCommand help = subCommands.get(0);
                help.perform(player,args);
            } else {
                for (SubCommand subCommand : this.getSubCommands()) {
                    // If the first argument matches the name of a subcommand
                    if (args[0].equalsIgnoreCase(subCommand.getName())) {
                        subCommand.perform(player, args);

                        return true;
                    }
                }
            }

        } else {
            plugin.getLogger().info("You need to be a player to execute this command");
        }

        return true;
    }

    public ArrayList<SubCommand> getSubCommands() {
        return subCommands;
    }
}
