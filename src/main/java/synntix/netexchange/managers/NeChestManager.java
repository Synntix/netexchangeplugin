package synntix.netexchange.managers;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.inventory.DoubleChestInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import synntix.netexchange.NetExchange;
import synntix.netexchange.chests.NeChest;
import synntix.netexchange.exceptions.ExchangeException;
import synntix.netexchange.utils.StackWithChest;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class NeChestManager {

    private DataManager dataManager;
    private MessageManager messageManager;
    private ConcurrentMap<Location, NeChest> neChestList;
    private HashMap<UUID, LinkedList<NeChest>> playersNeChests;

    public NeChestManager(DataManager dataManager, MessageManager messageManager) {
        this.dataManager = dataManager;
        this.messageManager = messageManager;
        this.neChestList = new ConcurrentHashMap<>();
        this.playersNeChests = new HashMap<>();
        getSavedNeChests();
    }

    /**
     * Gets the saved NeChests from config file and populate neChestList and playerNeChests with it
     */
    private void getSavedNeChests() {
        this.neChestList = this.dataManager.getSavedNeChests();

        for (NeChest neChest : neChestList.values()) {
            UUID playerUUID = UUID.fromString(neChest.getPlayerUUID());

            LinkedList<NeChest> playerList = playersNeChests.get(playerUUID);
            if (playerList == null) {
                playerList = new LinkedList<>();
                playersNeChests.put(playerUUID, playerList);
            }
            playerList.add(neChest);
        }
    }

    /**
     * Creates a NeChest and populate neChestList and playersNeChests with it
     * @param playerUUID
     * @param location
     * @return The created NeChest
     */
    public NeChest createNeChest(UUID playerUUID, Location location) {
        NeChest newNeChest = new NeChest(playerUUID.toString(), location);
        this.neChestList.put(location, newNeChest);
        LinkedList<NeChest> playerList = this.playersNeChests.get(playerUUID);
        if (playerList == null) {
            playerList = new LinkedList<>();
            this.playersNeChests.put(playerUUID, playerList);
        }
        playerList.add(newNeChest);

        saveNeChests();
        return newNeChest;
    }

    /**
     * Removes a NeChest at a given location from neChestList, playersNeChests and the dataConfig
     * @param location
     */
    public void removeNeChest(Location location) {
        NeChest chestToRemove = this.neChestList.get(location);
        this.getPlayerNeChestList(UUID.fromString(chestToRemove.getPlayerUUID())).remove(chestToRemove);
        this.neChestList.remove(location);
        this.dataManager.removeNeChest(location);
        saveNeChests();
    }

    public void saveNeChests() {
        for (NeChest neChest : this.neChestList.values()) {
            dataManager.saveNeChest(neChest);
        }
    }

    public boolean isChest(Block block) {
        return block.getType() == Material.CHEST || block.getType() == Material.TRAPPED_CHEST;
    }

    /**
     * Determines whether a chest at a given location is already owned, and if so,
     * if the owner is the uuid or someone else
     *
     * @param uuid The UUID to check whether it's the owner or not
     * @param chestLocation The location of the chest to check the ownership
     * @return 0 - If the chest isn't owned by someone, 1 - If owned by the uuid, 2 - If owned by someone else
     */
    public int isAlreadyOwned(String uuid, Location chestLocation) {

        NeChest neChest = this.neChestList.get(chestLocation);

        if (neChest == null) {
            return 0;
        } else if (neChest.getPlayerUUID().equals(uuid)) {
            return 1;
        } else {
            return 2;
        }

    }

    /**
     * Returns a LinkedList of all NeChests of a player
     * @param playerUUID
     * @return NeChests of the player
     */
    public LinkedList<NeChest> getPlayerNeChestList(UUID playerUUID) {
        LinkedList<NeChest> playerNeChestList = this.playersNeChests.get(playerUUID);
        if (playerNeChestList == null) {
            playerNeChestList = new LinkedList<>();
        }
        return playerNeChestList;
    }

    public NeChest getNeChest(Location location) {
        return this.neChestList.get(location);
    }

    public Location translateLocation(Location location) {
        Location newLocation = location;

        Block block = location.getBlock();
        if (block.getState() instanceof Chest) {
            Chest chest = (Chest) block.getState();
            if (chest.getInventory() instanceof DoubleChestInventory) {
                DoubleChest doubleChest = (DoubleChest) chest.getInventory().getHolder();
                newLocation = doubleChest.getLocation();
            }
        }

        return newLocation;
    }

    public boolean isNetExchangeContainer(Location location) {
        return this.neChestList.get(location) != null;
    }



    /**
     * Return all the ItemsStacks cumulated in every NEChest
     * of a player
     *
     * @param uuid The UUID of the player
     * @return An ArrayList of all the ItemStacks
     */
    public ArrayList<ItemStack> getNEInventory(String uuid) {
        ArrayList<ItemStack> chestList = new ArrayList<>();

        //If the player doesn't have any NEChest
        if (this.playersNeChests.get(UUID.fromString(uuid)) == null) {
            return chestList;
        }

        //For every NEChests
        for (NeChest nechest : this.playersNeChests.get(UUID.fromString(uuid))) {
                //For every ItemStack in chest
                for (ItemStack itemStack : nechest.getContents()) {
                    if (itemStack != null) {
                        chestList.add(itemStack);
                    }
                }
            }

        return chestList;

    }

    /**
     * Checks if the chest has a free ItemStack
     *
     * @param chest
     * @return True if the chest has a free ItemStack
     */
    public static boolean chestHasFreeSpace(Chest chest) {
        ItemStack[] chestInventory = chest.getInventory().getContents();
        int i = 0;
        while (i < chestInventory.length && chestInventory[i] != null) {
            i++;
        }
        return i != chestInventory.length;
    }

    /**
     * Returns the index of the first empty ItemStack of
     * the given chest, the chest MUST NOT BE FULL
     *
     * @param chest
     * @return Index of the first empty ItemStack
     */
    public static int findFreeSpaceInChest(Chest chest)  {
        ItemStack[] chestInventory = chest.getInventory().getContents();
        int i = 0;
        while (i < chestInventory.length && chestInventory[i] != null) {
            i++;
        }
        return i;
    }

    /**
     * Exchange an ItemStack from one chest to another,
     * the given ItemStack must be in chestFrom and chestTo
     * must not be empty
     *
     * @param chestFrom The chest to take the ItemStack from
     * @param chestTo The chest to give the ItemStack to
     * @param itemStack The ItemStack to be exchanged
     * @throws Exception If the ItemStack is not in chestFrom or if chestTo is full
     */
    public void exchangeItemStack(Chest chestFrom, Chest chestTo, ItemStack itemStack) throws ExchangeException {
        Inventory chestFromInventory = chestFrom.getInventory();
        Inventory chestToInventory = chestTo.getInventory();
        if (!chestFromInventory.contains(itemStack)) {
            throw new ExchangeException("The itemStack given was not in the chest");
        }
        if (!chestHasFreeSpace(chestTo)) {
            throw new ExchangeException("The destination chest is full");
        } else {
            chestFromInventory.removeItem(itemStack);
            int itemDestPos = findFreeSpaceInChest(chestTo);
            chestToInventory.setItem(itemDestPos,itemStack);
        }
    }

    public ArrayList<NeChest> getNeChestsWithFreeSpace(UUID playerUUID) {
        LinkedList<NeChest> playerChestList = this.playersNeChests.get(playerUUID);
        ArrayList<NeChest> chestsWithFreeSpace = new ArrayList<>();

        if (playerChestList == null || playerChestList.size() == 0) {
            return chestsWithFreeSpace;
        } else {
            for (NeChest neChest : playerChestList) {
                if (chestHasFreeSpace(neChest.getChest())) {
                    chestsWithFreeSpace.add(neChest);
                }
            }
        }

        return chestsWithFreeSpace;
    }

    public ArrayList<StackWithChest> getItemStackInNeInventory(UUID playerUUID, Material material) {
        ArrayList<StackWithChest> list = new ArrayList<>();
        LinkedList<NeChest> playerChestList = this.playersNeChests.get(playerUUID);

        if (playerChestList == null || playerChestList.size() == 0) {
            return null;
        } else {
            for (NeChest neChest : playerChestList) {
                Inventory inventory = neChest.getInventory();
                if (inventory.contains(material)) {
                    int i = 0;
                    while (i < inventory.getSize() && (inventory.getItem(i) == null || inventory.getItem(i).getType() != material)) {
                        i++;
                    }
                    if (i != inventory.getSize()) {
                        list.add(new StackWithChest(neChest, inventory.getItem(i)));
                    }
                }
            }
        }

        return list;
    }

}
