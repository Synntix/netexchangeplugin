package synntix.netexchange.managers;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import synntix.netexchange.NetExchange;
import synntix.netexchange.NetExchangeBot;
import synntix.netexchange.Pairing;
import synntix.netexchange.chests.NeChest;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;

public class DataManager {

    private NetExchange plugin;
    private NetExchangeBot netExchangeBot;
    private File dataFile;
    private File pairingFile;
    private YamlConfiguration dataConfig;
    private YamlConfiguration pairingConfig;

    public DataManager(NetExchange netExchange, NetExchangeBot netExchangeBot) {
        this.plugin = netExchange;
        this.netExchangeBot = netExchangeBot;
        createDataFile();
    }

    private void createDataFile() {
        this.dataFile = new File(plugin.getDataFolder(),"NEchests.yml");
        this.pairingFile = new File(plugin.getDataFolder(),"Pairings.yml");

        try {
            if (dataFile.createNewFile()) {
                plugin.getLogger().log(Level.INFO, "Creating NeChests config file");
            }
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "An error occurred while attempting to create NeChests config file.");
            e.printStackTrace();
        }

        try {
            if (pairingFile.createNewFile()) {
                plugin.getLogger().log(Level.INFO, "Creating Pairings config file");
            }
        } catch (Exception e) {
            plugin.getLogger().log(Level.SEVERE, "An error occurred while attempting to create Pairings config file.");
            e.printStackTrace();
        }

        this.dataConfig = YamlConfiguration.loadConfiguration(dataFile);
        this.pairingConfig = YamlConfiguration.loadConfiguration(pairingFile);
    }

    public void saveData() {
        try {
            dataConfig.save(dataFile);
            pairingConfig.save(pairingFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveNeChest(NeChest neChest) {
        if (neChest.getLocation().getWorld() != null) {
            String world = neChest.getLocation().getWorld().getName();
            int x = neChest.getLocation().getBlockX();
            int y = neChest.getLocation().getBlockY();
            int z = neChest.getLocation().getBlockZ();

            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".playerUUID", neChest.getPlayerUUID());
            dataConfig.set(world + "." + x + "_" + y +"_" + z + ".name", neChest.getName());

            saveData();
        }
    }

    public void removeNeChest(Location location) {
        String worldString = location.getWorld().getName();
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();

        dataConfig.set(worldString + "." + x + "_" + y + "_" + z, null);
        saveData();

    }

    public ConcurrentMap<Location, NeChest> getSavedNeChests() {
        ConcurrentMap<Location, NeChest> neChestList = new ConcurrentHashMap<>();

        for (String worldString: dataConfig.getKeys(false)) {
            for (String coordsString : Objects.requireNonNull(dataConfig.getConfigurationSection(worldString)).getKeys(false)) {
                World world = plugin.getServer().getWorld(worldString);

                String[] coords = coordsString.split("_");

                int x = Integer.parseInt(coords[0]);
                int y = Integer.parseInt(coords[1]);
                int z = Integer.parseInt(coords[2]);

                Location location = new Location(world, x, y, z);

                String playerUUID = dataConfig.getString(worldString + "." + coordsString + ".playerUUID");

                String name = dataConfig.getString(worldString + "." + coordsString + ".name");

                NeChest neChest = new NeChest(playerUUID, location, name);

                neChestList.put(location, neChest);

            }
        }
        saveData();

        return neChestList;
    }

    public void savePairing(Pairing p) {
        pairingConfig.set(p.getMcID() + ".mcName", p.getMcName());
        pairingConfig.set(p.getMcID() + ".discordID", p.getDiscordID());
        pairingConfig.set(p.getMcID() + ".discordTag", p.getDiscordTag());
        pairingConfig.set(p.getMcID() + ".guildID", p.getGuildID());
        pairingConfig.set(p.getMcID() + ".messageID", p.getMessageID());
        pairingConfig.set(p.getMcID() + ".confirmed", p.isConfirmed());

        saveData();
    }

    public void removePairing(Pairing p) {
        pairingConfig.set(p.getMcID(), null);

        saveData();
    }

    public ArrayList<Pairing> getSavedPairings() {
        ArrayList<Pairing> pairingList = new ArrayList<>();

        for (String mcUUID : pairingConfig.getKeys(false)) {
            String mcName = pairingConfig.getString(mcUUID + ".mcName");
            String discordID = pairingConfig.getString(mcUUID + ".discordID");
            String discordTag = pairingConfig.getString(mcUUID + ".discordTag");
            String guildId = pairingConfig.getString(mcUUID + ".guildId");
            String messageId = pairingConfig.getString(mcUUID + ".messageID");
            boolean confirmed = Boolean.parseBoolean(pairingConfig.getString(mcUUID + ".confirmed"));


            Pairing newPairing = new Pairing(netExchangeBot, mcUUID, mcName, discordTag, guildId);

            if (confirmed) {
                newPairing.confirm();
            }

            newPairing.setMessageID(messageId);
            newPairing.setDiscordID(discordID);

            pairingList.add(newPairing);
        }

        return pairingList;
    }

}
