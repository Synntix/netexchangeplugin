package synntix.netexchange.botlisteners;

import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.react.PrivateMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import synntix.netexchange.NetExchangeBot;
import synntix.netexchange.Pairing;
import synntix.netexchange.managers.DataManager;

public class ReactionListener extends ListenerAdapter {

    private NetExchangeBot netExchangeBot;
    private DataManager dataManager;

    public ReactionListener(NetExchangeBot netExchangeBot, DataManager dataManager) {
        this.netExchangeBot = netExchangeBot;
        this.dataManager = dataManager;
    }

    @Override
    public void onPrivateMessageReactionAdd(@NotNull PrivateMessageReactionAddEvent event) {
        System.out.println("[PrivateMessageReactionListener] Reaction added : "+event.getReactionEmote());
        User author = event.getUser();
        if (author != null) {// Author may be null if not cached
            if (!author.getId().equals(netExchangeBot.getUserID())) {
                Pairing p = netExchangeBot.getPairingByDiscordID(author.getId());
                // Check if the retrieved pairing exists and is not already confirmed
                if (p != null && !p.isConfirmed()) {
                    System.out.println("[PrivateMessageReactionListener] Pairing found, with message ID "+p.getMessageID()+" | Reaction message ID "+event.getMessageId());
                    if (event.getMessageId().equals(p.getMessageID())) {
                        // In THAT specific case, the reaction is on the Pairing request message
                        // We're using the toString() to test the emoji, because for some reason getName() and getAsReactionCode() output an empty string...
                        if (event.getReactionEmote().toString().equals("RE:U+2705")) {
                            // This is a "check_mark" which means PAIRING CONFIRMED!
                            p.confirm();
                            dataManager.savePairing(p);
                            event.getChannel().sendMessage("Pairing request CONFIRMED!\nYou are now linked to Minecraft account '" + p.getMcName() + "'.").queue();
                        } else {
                            netExchangeBot.removePairing(p);
                            dataManager.removePairing(p);
                            event.getChannel().sendMessage("Pairing request CANCELLED\n*If you reacted by mistake, please log into Minecraft and resend a pairing command.*").queue();
                        }
                    }
                } else {
                    System.err.println("[PrivateMessageReactionListener] No pending pairing found for author '"+author.getAsTag()+"'");
                }
            } else {
                System.out.println("[PrivateMessageReactionListener] Reaction added by bot, ignoring");
            }
        } else {
            System.err.println("[PrivateMessageReactionListener] Couldn't get author, User is probably not in cache");
        }
    }

//    @Override
//    public void onPrivateMessageReceived(@NotNull PrivateMessageReceivedEvent event) {
//        System.out.println("Private message received from "+event.getAuthor().getAsTag());
//    }
}