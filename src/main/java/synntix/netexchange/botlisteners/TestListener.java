package synntix.netexchange.botlisteners;

import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import synntix.netexchange.NetExchangeBot;

public class TestListener extends ListenerAdapter {

    @Override
    public void onReady(@NotNull ReadyEvent event) {

        System.out.println("[TestListener] Bot is ready! Waiting for commands...");
        // Request the loading of all guild members, so that they are kept in cache and can be found using their Discord Tag
        event.getJDA().getGuildById("353831162260488192").loadMembers();
        System.out.println("[TestListener] Members loading started");
    }

//    @Override
//    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
//        NetExchangeBot.addPairing("353831162260488192","TickingFeather","TickingFeather#0128","353831162260488192");
//    }
}