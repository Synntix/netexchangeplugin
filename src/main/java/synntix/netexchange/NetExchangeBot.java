package synntix.netexchange;

import com.jagrosh.jdautilities.command.CommandClient;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.MemberCachePolicy;
import synntix.netexchange.botcommands.CmdList;
import synntix.netexchange.botcommands.CmdSend;
import synntix.netexchange.botlisteners.ReactionListener;
import synntix.netexchange.botlisteners.TestListener;
import synntix.netexchange.managers.DataManager;
import synntix.netexchange.managers.NeChestManager;

import java.util.ArrayList;
import java.util.function.Consumer;

public class NetExchangeBot {
    private NetExchange plugin;
    private NeChestManager neChestManager;
    private DataManager dataManager;
    private JDA api;
    private CommandClient cmdclient;
    private String userID;
    private ArrayList<Pairing> pairings;

    public NetExchangeBot(NetExchange plugin, NeChestManager neChestManager, DataManager dataManager) {
        this.plugin = plugin;
        this.neChestManager = neChestManager;
        this.dataManager = dataManager;
    }

    public void run(String token, String botUserID) throws javax.security.auth.login.LoginException {
        // Save the user ID (useful to avoid responding to the bot's own actions
        userID=botUserID;
        // Initialize the pairings list (empty)
        pairings = new ArrayList<>();
        getSavedPairings();

        // Create the Command Client Builder
        CommandClientBuilder cmdbuilder = new CommandClientBuilder();

        // Set various information about the bot
        cmdbuilder.setOwnerId(botUserID);
        cmdbuilder.setPrefix("!ne");
        cmdbuilder.setActivity(Activity.listening("!ne help"));
        // Add commands to the Command Builder
        cmdbuilder.addCommands(new CmdList(neChestManager, this));
        cmdbuilder.addCommands(new CmdSend(plugin, neChestManager, this));

        // Command Client is filled, we can build it
        cmdclient = cmdbuilder.build();

        // Create and configure the JDA Builder
        api = JDABuilder.createDefault(token)
        // All these methods can be chained because they return the JDABuilder object they are called on
                .addEventListeners(cmdclient, new TestListener(), new ReactionListener(this, dataManager))// Add listeners
                .enableIntents(GatewayIntent.GUILD_MEMBERS)// Enable privileged GatewayIntent needed to load all guild members
                .setMemberCachePolicy(MemberCachePolicy.ALL)// ..and set the Member Cache Policy to keep all members in cache. This is necessary to be able to getMembers() and find the one with a specific Name#Discriminator among them
                // Finally, build the JDA object
                .build();

    }

    public JDA getApi() { return this.api; }


    public void sendAndReact(User user, String messageBody) {
        Consumer<Message> callback = sentmsg -> sentmsg.addReaction("U+2705").queue();
        user.openPrivateChannel().queue(channel -> channel.sendMessage(messageBody).queue(callback));
    }

    public String getUserID() {
        return this.userID;
    }

    public void addPairing(String mcID, String mcName, String discordName, String guildID) {
        // We need to add a pairing only if there is no pairing for this user
        if (getPairingByMCID(mcID) == null) {
            // Create new pairing
            Pairing newpairing = new Pairing(this, mcID,mcName,discordName,guildID);
            // Add it to the pairings list
            pairings.add(newpairing);
            dataManager.savePairing(newpairing);
            System.out.printf("[NetExchangeBot] Added pairing between (MC:%s|%s) and (Discord:%s) based on guild %s%n",mcName,mcID,discordName,guildID);
            // And request a confirmation (asks user via Discord DM)
            newpairing.requestConfirmation();
        }
    }

    public void removePairing(Pairing p) {
        pairings.remove(p);
    }

    public Pairing getPairing(int index) {
        return pairings.get(index);
    }

    public Pairing getPairingByDiscordID(String discordID) {
        int i=0;
        while (i < pairings.size() && !pairings.get(i).getDiscordID().equals(discordID)) {
            i++;
        }
        if (i < pairings.size() && pairings.get(i).getDiscordID().equals(discordID)) {
            return pairings.get(i);
        } else {
            return null;
        }
    }

    public Pairing getPairingByMCID(String mcID) {
        int i=0;
        while (i < pairings.size() && !pairings.get(i).getMcID().equals(mcID)) {
            i++;
        }
        if (i < pairings.size() && pairings.get(i).getMcID().equals(mcID)) {
            return pairings.get(i);
        } else {
            return null;
        }
    }

    public void savePairings() {
        for (Pairing p : this.pairings) {
            dataManager.savePairing(p);
        }
    }

    private void getSavedPairings() {
        this.pairings.addAll(dataManager.getSavedPairings());
    }
}
