# NetExchange
**NetExchange** (NE) is a Minecraft plugin which aims to allow players to exchange in-game items *via* an external interface (Discord or an Android app).  **NetExchange is not fully-implemented.**

## Build
### Requirements

- Maven

### Instructions
1. Clone project from gitlab: `git clone https://gitlab.com/Synntix/netexchangeplugin.git`
2. Create the .jar with `mvn clean package`
3. The plugin is now in the target folder.

## Development

- [x] Adding a NEChest
- [x] Removing a NEChest by breaking it
- [ ] Removing a NEChest with a command
- [x] NEChests persistence
- [x] Moving items between chests

### Discord side

- [x] Linking a Minecraft account with a Discord account
- [ ] Making that link permanent 
- [ ] Listing NE inventory content
	- [ ] Inventory sorting / search
- [ ] Trading with another player
	- [ ] Creating a trade
	- [ ] Adding items to trade
	- [ ] Confirming a trade
	- [ ] Cancelling a trade
- [ ] Listing content of individual chests
- [ ] Moving items between a player's chests
